﻿using Microsoft.EntityFrameworkCore;

namespace SpectacledBear.Hobbit.Data;

public class HobbitContext : DbContext
{
    public const string ConnectionString = "DataSource=Hobbits;mode=memory;cache=shared";
    
    public DbSet<Hobbit>? Hobbits { get; set; } = null;

    public void LoadHobbits()
    {
        using var dbContext = new HobbitContext();
        
        dbContext.Add(new Hobbit("Frodo", "Baggins", 1368, 1421));
        dbContext.Add(new Hobbit("Samwise", "Gamgee", 1380, 1482));
        
        dbContext.SaveChanges();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite(ConnectionString);
}
