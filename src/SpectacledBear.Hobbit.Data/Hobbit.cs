﻿namespace SpectacledBear.Hobbit.Data;

public class Hobbit
{
    public long Id { get; set; }
    public string GivenName { get; set; }
    public string FamilyName { get; set; }
    public int BirthYear { get; set; }
    public int? DeathYear { get; set; }

    public Hobbit(
        string givenName,
        string familyName,
        int birthYear,
        int? deathYear = null)
    {
        GivenName = givenName;
        FamilyName = familyName;
        BirthYear = birthYear;
        DeathYear = deathYear;
    }
}
