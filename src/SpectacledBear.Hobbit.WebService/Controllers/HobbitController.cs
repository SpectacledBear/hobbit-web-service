using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SpectacledBear.Hobbit.Data;

namespace SpectacledBear.Hobbit.WebService.Controllers;

[ApiController]
[Route("[controller]")]
public class HobbitController : ControllerBase
{
    private readonly HobbitContext _context;

    private readonly ILogger<HobbitController> _logger;

    public HobbitController(HobbitContext context, ILogger<HobbitController> logger)
    {
        _context = context;
        _logger = logger;
    }

    public async Task<ActionResult<IEnumerable<SpectacledBear.Hobbit.Data.Hobbit>>> GetHobbits()
    {
        if (_context.Hobbits == null)
        {
            return NotFound();
        }
    
        return await _context.Hobbits.ToListAsync();
    }
}
