using Microsoft.Data.Sqlite;
using SpectacledBear.Hobbit.Data;

var builder = WebApplication.CreateBuilder(args);

using var keepAliveConnection = new SqliteConnection(HobbitContext.ConnectionString);
keepAliveConnection.Open();

using var dbContext = new HobbitContext();
dbContext.Database.EnsureCreated();
dbContext.LoadHobbits();

if (dbContext.Hobbits == null || dbContext.Hobbits.Count() < 1) {
    throw new ContextMarshalException("The HobbitContext did not load starting hobbits.");
}

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddDbContext<HobbitContext>();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
